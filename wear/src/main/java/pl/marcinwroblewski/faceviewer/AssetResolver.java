package pl.marcinwroblewski.faceviewer;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

/**
 * Created by Admin on 06.10.2016.
 */

public class AssetResolver {

    private GoogleApiClient googleApiClient;
    private final String TAG = "AssetResolver";

    public AssetResolver(GoogleApiClient googleApiClient) {
        this.googleApiClient = googleApiClient;
    }

    public JSONArray resolveAssetToJSONArray(Asset asset) throws IOException, JSONException {
        if (asset == null) {
            throw new IllegalArgumentException("Asset must be non-null");
        }
        ConnectionResult result =
                googleApiClient.blockingConnect(1000 * 15, TimeUnit.MILLISECONDS);
        if (!result.isSuccess()) {
            return null;
        }
        // convert asset into a file descriptor and block until it's ready
        InputStream assetInputStream = Wearable.DataApi.getFdForAsset(
                googleApiClient, asset).await().getInputStream();
        googleApiClient.disconnect();

        if (assetInputStream == null) {
            Log.w(TAG, "Requested an unknown Asset.");
            return null;
        }

        return new JSONArray(inputSteamToString(assetInputStream));
    }

    private String inputSteamToString(InputStream inputStream) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();

        Log.d(TAG, "inputStream to String conversion");

        return new String(buffer.toByteArray(), StandardCharsets.UTF_8);
    }
}

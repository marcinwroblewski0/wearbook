package pl.marcinwroblewski.faceviewer;

import com.google.android.gms.wearable.DataMap;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wrbl on 23.11.16.
 */

public class MessagesUtility {

    public static String extractMessageForUser(DataMap dataMap) throws JSONException {

        JSONObject errorJSON = new JSONObject(dataMap.getString("info"));
        return errorJSON.getString("message");
    }

}

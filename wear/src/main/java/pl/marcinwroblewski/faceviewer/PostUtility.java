package pl.marcinwroblewski.faceviewer;

import com.google.android.gms.wearable.DataMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 05.11.2016.
 */

public class PostUtility {



    public static JSONArray dataMapToPosts(DataMap dataMap) throws JSONException {
        JSONArray posts = new JSONArray(dataMap.getString("posts"));
        return posts;
    }

    public static String[][] postsToStringArray(JSONArray posts) throws JSONException {
        String[][] data = new String[posts.length()][2];

        for(int i = 0; i < posts.length(); i++) {
            JSONObject post = null;
            post = posts.getJSONObject(i);

            data[i][0] = post.getString("author");
            if(post.has("text"))
                data[i][1] = post.getString("text");
            else
                data[i][1] = ":";
        }

        return data;
    }
}

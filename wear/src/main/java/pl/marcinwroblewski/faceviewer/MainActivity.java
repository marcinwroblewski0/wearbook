package pl.marcinwroblewski.faceviewer;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.support.wearable.view.GridViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Random;

public class MainActivity extends Activity implements DataApi.DataListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private Node mNode;
    private GoogleApiClient mGoogleApiClient;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private AVLoadingIndicatorView loadingIndicatorView, errorIndicatorView;
    private TextView messageTV;
    private boolean displayingError;

    String[][] data = {};
    GridViewPager pager;

    private LinearLayout worryMessage;

    private Runnable worryMessageShowRunnable = new Runnable() {
        @Override
        public void run() {
            worryMessage.setVisibility(View.VISIBLE);
        }
    };

    private Handler worryMessageHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupSharedPreferences();
        setupGoogleApiClient();
        setupViews();
    }

    private void setupViews() {
        pager = (GridViewPager) findViewById(R.id.grid_view_pager);
        pager.setAdapter(new GridPagerAdapter(getFragmentManager(), data));

        worryMessage = (LinearLayout)findViewById(R.id.worry_message);
        worryMessageHandler.postDelayed(worryMessageShowRunnable, 1000 * 30);

        loadingIndicatorView = (AVLoadingIndicatorView) findViewById(R.id.loading_spinner);
        setRandomIndicator();

        errorIndicatorView = (AVLoadingIndicatorView) findViewById(R.id.error_spinner);
        messageTV = (TextView) findViewById(R.id.message);
    }

    private void setupSharedPreferences() {
        preferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        editor = preferences.edit();
    }

    private void setupGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private void sendUpdateRequest() {
        if(mNode != null)
        Wearable.MessageApi.sendMessage(mGoogleApiClient, mNode.getId(), "/request", "update".getBytes())
                .setResultCallback(new ResultCallback<MessageApi.SendMessageResult>() {
                    @Override
                    public void onResult(@NonNull MessageApi.SendMessageResult sendMessageResult) {
                        if(sendMessageResult.getStatus().isSuccess()) {
                            Log.d("Request update", "Send");
                        } else {
                            Log.e("Request update", "Fail: " + sendMessageResult.getStatus().getStatusMessage());
                        }
                    }
                });
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {

        for (DataEvent event : dataEventBuffer) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                handleDataEventChange(event);
            }
        }
    }

    private void handleDataEventChange(DataEvent event) {
        DataItem item = event.getDataItem();
        DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
        if (item.getUri().getPath().compareTo(DataMapRoutes.POSTS) == 0) {
            displayPosts(dataMap);

        } else if (item.getUri().getPath().compareTo(DataMapRoutes.ERRORS) == 0) {
            try {
                displayErrorInfo(MessagesUtility.extractMessageForUser(dataMap));
            } catch (JSONException e) {
                displayErrorInfo("Error while displaying error");
                e.printStackTrace();
            }

        } else if (item.getUri().getPath().compareTo(DataMapRoutes.INFO) == 0) {
            try {
                displayInfo(MessagesUtility.extractMessageForUser(dataMap));
            } catch (JSONException e) {
                displayErrorInfo("Error while displaying info");
                e.printStackTrace();
            }
        }
    }

    private void removeWorryHandlerCallbacks() {
        worryMessageHandler.removeCallbacks(worryMessageShowRunnable);
        worryMessage.setVisibility(View.GONE);
    }

    private void hideSpinner() {
        View spinner = findViewById(R.id.loading_spinner);
        spinner.setVisibility(View.GONE);

        errorIndicatorView.setVisibility(View.GONE);
    }

    private void displayErrorInfo(String message) {
        displayingError = true;
        messageTV.setText(message);

        loadingIndicatorView.setVisibility(View.GONE);
        errorIndicatorView.setVisibility(View.VISIBLE);

        removeWorryHandlerCallbacks();
    }

    private void displayInfo(String message) {
        messageTV.setText(message);

        loadingIndicatorView.setVisibility(View.VISIBLE);
        errorIndicatorView.setVisibility(View.GONE);
    }

    private void displayPosts(DataMap dataMap) {
        try {
            JSONArray posts = PostUtility.dataMapToPosts(dataMap);
            data = PostUtility.postsToStringArray(posts);

            pager.setAdapter(new GridPagerAdapter(getFragmentManager(), data));
        } catch (JSONException e) {
            displayErrorInfo("Can't decode data from handheld");
            e.printStackTrace();
        }

        hideSpinner();
        removeWorryHandlerCallbacks();
        hideMessage();
    }

    private void hideMessage() {
        messageTV.setVisibility(View.GONE);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if(bundle != null)
            Log.d("Connection", bundle.toString());

        Log.d("Connection", "Connected");
        Wearable.DataApi.addListener(mGoogleApiClient, this);

        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient)
                .setResultCallback(new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                    @Override
                    public void onResult(@NonNull NodeApi.GetConnectedNodesResult nodes) {
                        for(Node node : nodes.getNodes()) {
                            if(node != null && node.isNearby()) {
                                mNode = node;
                                Log.d("Connected", "to " + node.getDisplayName());
                                sendUpdateRequest();
                            }
                        }
                    }
                });

    }

    private void setRandomIndicator() {
        Log.d("change", "called");
        String[] indicatorsNames = {"LineScaleIndicator", "LineScalePartyIndicator", "LineScalePulseOutIndicator", "LineScalePulseOutRapidIndicator"};
        Random random = new Random();
        loadingIndicatorView.setIndicator(indicatorsNames[random.nextInt(indicatorsNames.length)]);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d("Connection", i + "");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.d("Connection", connectionResult.getErrorMessage() + "");
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        Wearable.DataApi.removeListener(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(displayingError) sendUpdateRequest();
    }

    private static final class GridPagerAdapter extends FragmentGridPagerAdapter {

        String[][] mData;

        private GridPagerAdapter(FragmentManager fm, String[][] data) {
            super(fm);
            mData = data;
        }

        @Override
        public Fragment getFragment(int row, int column) {
            return (CardFragment.create(mData[row][0], mData[row][1]));
        }

        @Override
        public int getRowCount() {
            return mData.length;
        }

        @Override
        public int getColumnCount(int row) {
            return 1;
        }
    }


}



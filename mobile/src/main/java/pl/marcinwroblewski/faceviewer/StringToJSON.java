package pl.marcinwroblewski.faceviewer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 17.11.2016.
 */

public class StringToJSON {

    public static JSONArray makeJSONArray(String raw) throws JSONException {
        JSONObject responseJSON = new JSONObject(raw);
        return responseJSON.getJSONArray("data");
    }

}

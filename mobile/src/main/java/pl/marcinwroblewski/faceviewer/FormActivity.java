package pl.marcinwroblewski.faceviewer;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;

public class FormActivity extends AppCompatActivity {

    private String username = "I ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        FacebookSdk.sdkInitialize(getApplicationContext());

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6300807210299804~4354805972");
        NativeExpressAdView mAdView = (NativeExpressAdView) findViewById(R.id.adview_native);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("FBBFB6C53626BBC62C0C049AA4DF505B")
                .build();
        mAdView.loadAd(adRequest);

        Button sendButton = (Button) findViewById(R.id.button_send);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEmailApp();
            }
        });

        if(Profile.getCurrentProfile() != null) {
            username = Profile.getCurrentProfile().getFirstName();
        }
    }

    public void openEmailApp() {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "nicram606@gmail.com", null));
        String versionName = "";
        try {
            versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException ignored) {}

        intent.putExtra(Intent.EXTRA_SUBJECT, username + " want to talk about Wearbook " + versionName);
        startActivity(Intent.createChooser(intent, "Send feedback"));
    }

}

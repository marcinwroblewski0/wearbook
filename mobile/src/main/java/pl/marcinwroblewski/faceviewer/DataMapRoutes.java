package pl.marcinwroblewski.faceviewer;

/**
 * Created by wrbl on 23.11.16.
 */

public class DataMapRoutes {

    public static String POSTS = "/posts";
    public static String ERRORS = "/errors";
    public static String INFO = "/info";

}

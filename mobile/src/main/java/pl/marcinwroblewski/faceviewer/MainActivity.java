package pl.marcinwroblewski.faceviewer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;


public class MainActivity extends AppCompatActivity {

    CallbackManager callbackManager;
    InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-6300807210299804/8998090774");
        requestNewInterstitial();


        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6300807210299804~4354805972");
        NativeExpressAdView mAdView = (NativeExpressAdView) findViewById(R.id.adview_native);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("FBBFB6C53626BBC62C0C049AA4DF505B")
                .build();
        mAdView.loadAd(adRequest);

        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_posts", "user_friends", "user_likes", "user_managed_groups");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                try {
                    Log.d("FacebookSDK", loginResult.getAccessToken().getPermissions().toString());
                    Profile profile = Profile.getCurrentProfile();
                    Log.d("FacebookSDK", profile.getName());
                } catch (NullPointerException e) {
                    Log.d("MainActivity", "onSuccess null pointer");
                    e.printStackTrace();
                } finally {
                    startActivity(new Intent(getApplicationContext(), SyncActivity.class));
                    finish();
                }
            }

            @Override
            public void onCancel() {
                Log.d("FacebookSDK", "Canceled");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("FacebookSDK", error.toString());
            }
        });

        if(Profile.getCurrentProfile() != null) {
            startActivity(new Intent(getApplicationContext(), SyncActivity.class));
            finish();
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("FBBFB6C53626BBC62C0C049AA4DF505B")
                .build();

        interstitialAd.loadAd(adRequest);

    }

    int counter = 0;
    @Override
    public void onBackPressed() {
        counter++;
        if(counter % 2 != 0) {
            if(interstitialAd.isLoaded()) {
                interstitialAd.show();
            }
        } else {
            super.onBackPressed();
        }
    }
}

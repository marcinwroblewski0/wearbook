package pl.marcinwroblewski.faceviewer;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Random;

/**
 * Created by Marcin Wróblewski on 06.10.16.
 */

public class ArrayMixer {

    private Random random;

    public ArrayMixer() {
        random = new Random();
    }

    public JSONArray mix(JSONArray array) {

        for(int i = 0; i < array.length(); i++) {
            try {
                int randomized = random.nextInt(array.length());
                Object firstElement = array.get(i);
                Object secondElement = array.get(randomized);
                array.put(i, secondElement);
                array.put(randomized, firstElement);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return array;
    }

}

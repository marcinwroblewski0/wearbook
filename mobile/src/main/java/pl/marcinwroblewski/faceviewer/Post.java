package pl.marcinwroblewski.faceviewer;

/**
 * Created by Admin on 05.11.2016.
 */

public class Post {

    private String title, content;


    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package pl.marcinwroblewski.faceviewer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 17.11.2016.
 */

public class JSONToPosts {

    public static JSONObject extractPost(JSONObject facebookResponse, String pageName) throws JSONException {

        JSONObject data = new JSONObject();
        data.put("author", pageName);
        if(facebookResponse.has("message") || facebookResponse.has("story")) {
            if (facebookResponse.has("message"))
                data.put("text", facebookResponse.get("message"));
            else
                data.put("text", facebookResponse.get("story"));
        } else {
            data.put("text", "");
        }
        data.put("timestamp", System.currentTimeMillis());
        return data;
    }

}

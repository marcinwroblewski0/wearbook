package pl.marcinwroblewski.faceviewer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PostsPreviewActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private JSONArray posts;
    private int pagesFeedCollected, pagesCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_posts_preview);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        posts = new JSONArray();
    }

    @Override
    protected void onStart() {
        super.onStart();
        startDataSync();
    }

    public void startDataSync() {
        if(Profile.getCurrentProfile() != null) {
            Log.d("fb", Profile.getCurrentProfile().getName());

            //likes
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/likes?limit=999",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            try {
                                JSONObject responseJSON = new JSONObject(response.getRawResponse());
                                Log.d("Likes", responseJSON.getJSONObject("paging").toString(2));
                                JSONArray dataArrayJSON = (JSONArray) responseJSON.get("data");
                                pagesCount = dataArrayJSON.length() - 1;
                                Log.d("Pages", "count: " + dataArrayJSON.length());
                                for (int i = 0; i < dataArrayJSON.length(); i++) {
                                    JSONObject page = (JSONObject) dataArrayJSON.get(i);
                                    gatherDataAndSend(page.get("id").toString(), page.get("name").toString());
                                    Log.d("Pages", "id: " + page.getString("id") + ", name: " + page.getString("name"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
        }

    }

    public void gatherDataAndSend(String id, final String name) {
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + id + "/feed",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        handleFeedResponse(response, name);
                    }
                }
        ).executeAsync();
    }

    private void handleFeedResponse(GraphResponse response, String pageName) {
        Log.d("Timeline", response.getRawResponse());
        try {
            JSONArray dataArrayJSON = StringToJSON.makeJSONArray(response.getRawResponse());
            for (int i = 0; i < dataArrayJSON.length(); i++)
                posts.put(JSONToPosts.extractPost(dataArrayJSON.getJSONObject(i), pageName));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pagesFeedCollected++;
        if(pagesFeedCollected == pagesCount) {
            try {
                displayPosts();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void gatherAllLikes() {
        final JSONArray ids = new JSONArray();

        //likes
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/likes?limit=1500",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            JSONObject responseJSON = new JSONObject(response.getRawResponse());
                            Log.d("Likes", responseJSON.getJSONObject("paging").toString(2));
                            JSONArray dataArrayJSON = (JSONArray) responseJSON.get("data");
                            Log.d("Pages", "count: " + dataArrayJSON.length());
                            for (int i = 0; i < dataArrayJSON.length(); i++) {
                                JSONObject page = (JSONObject) dataArrayJSON.get(i);
                                Log.d("Pages", "id: " + page.getString("id") + ", name: " + page.getString("name"));
                                ids.put(page);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();

        LikesList.set(getApplicationContext(), ids);
        startDataSync();
    }



    public void displayPosts() throws JSONException {
        View progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        List<Post> postsList = new ArrayList<>();
        for(int i = 0; i < posts.length(); i++) {
            JSONObject post = posts.getJSONObject(i);
            Post postObject = new Post(post.getString("author"), post.getString("text"));
            postsList.add(postObject);
        }
        PostsAdaper postsAdaper = new PostsAdaper(postsList);
        recyclerView.setAdapter(postsAdaper);
    }
}

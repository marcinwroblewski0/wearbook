package pl.marcinwroblewski.faceviewer;

import com.google.android.gms.wearable.Asset;

import org.json.JSONArray;

/**
 * Created by Marcin Wróblewski on 06.10.2016.
 */

public class AssetCreator {

    public Asset createAssetFromJSONArray(JSONArray array) {
        return Asset.createFromBytes(array.toString().getBytes());
    }
}

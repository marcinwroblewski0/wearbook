package pl.marcinwroblewski.faceviewer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.NativeExpressAdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;


public class SyncActivity extends AppCompatActivity implements DataApi.DataListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private final String TAG = "FBHandheld";
    GoogleApiClient mGoogleApiClient;
    private JSONArray posts;
    CallbackManager callbackManager;
    int pagesFeedCollected, pagesCount;
    InterstitialAd interstitialAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_sync);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {

                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d(TAG, "onConnected: " + connectionHint);
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                // Request access only to the Wearable API
                .addApi(Wearable.API)
                .build();




        posts = new JSONArray();

        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-6300807210299804~4354805972");

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-6300807210299804/8998090774");
        requestNewInterstitial();

        NativeExpressAdView mAdView = (NativeExpressAdView) findViewById(R.id.adview_native);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("FBBFB6C53626BBC62C0C049AA4DF505B")
                .build();
        mAdView.loadAd(adRequest);


        View previewPostsButton = findViewById(R.id.preview_button);
        previewPostsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), PostsPreviewActivity.class));
            }
        });

        startDataSync();
    }

    private JSONArray preparePosts(JSONArray posts) {
        Log.d(TAG, "Data: " + posts.toString());
        ArrayMixer arrayMixer = new ArrayMixer();
        posts = arrayMixer.mix(posts);

        ArraySlicer arraySlicer = new ArraySlicer();
        try {
            posts = arraySlicer.slice(posts, 100);
        } catch (JSONException e) {
            Log.e(TAG, "Can't slice posts array");
            e.printStackTrace();
        }
        Log.d(TAG, "Mixed and sliced data: " + posts.toString());

        return posts;
    }

    private void sendPosts(JSONArray posts) {
        posts = preparePosts(posts);

        Log.d(TAG, "Data send: " + posts.toString());

        PutDataMapRequest putDataMapReq = PutDataMapRequest.create("/posts");
        putDataMapReq.getDataMap().putString("posts", posts.toString());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);
    }

    public void startDataSync() {
        if(Profile.getCurrentProfile() != null) {
            Log.d("fb", Profile.getCurrentProfile().getName());

            //likes
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/likes",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Log.d("Page", response.getRawResponse() + "");
                            try {
                                JSONObject responseJSON = (JSONObject) new JSONTokener(response.getRawResponse()).nextValue();
                                JSONArray dataArrayJSON = (JSONArray) responseJSON.get("data");
                                pagesCount = dataArrayJSON.length();
                                for (int i = 0; i < dataArrayJSON.length(); i++) {

                                    JSONObject page = (JSONObject) dataArrayJSON.get(i);
                                    gatherPosts(page.get("id").toString(), page.get("name").toString());
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
        }

        gatherGroupIds();
    }

    int counter = 0;
    public void gatherPosts(String id, final String name) {
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + id + "/feed",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        handleFeedResponse(response, name);
                    }
                }
        ).executeAsync();
    }


    private void handleFeedResponse(GraphResponse response, String pageName) {
        Log.d("Timeline", response.getRawResponse());
        try {
            JSONArray dataArrayJSON = StringToJSON.makeJSONArray(response.getRawResponse());
            for (int i = 0; i < dataArrayJSON.length(); i++)
                posts.put(JSONToPosts.extractPost(dataArrayJSON.getJSONObject(i), pageName));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        pagesFeedCollected++;
        if(pagesFeedCollected == 1 || pagesFeedCollected == pagesCount) {
            sendPosts(posts);
        }
    }

    public void gatherGroupIds() {
        Log.d(TAG, "gatherGroupIds called");
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me/groups",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        Log.d("groups", response.getRawResponse());
                        try {
                            JSONObject responseJSON = (JSONObject) new JSONTokener(response.getRawResponse()).nextValue();
                            JSONArray dataArrayJSON = (JSONArray) responseJSON.get("data");
                            for (int i = 0; i < dataArrayJSON.length(); i++) {
                                Log.d("groups", dataArrayJSON.get(i).toString());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        counter++;

                        if(counter % 5 == 0)
                            sendPosts(posts);

                        Log.d(TAG, "Groups counter: " + counter);
                    }
                }
        ).executeAsync();
    }

    private boolean isLoggedOut() {
        if(Profile.getCurrentProfile() == null) {
            startActivity(new Intent(getBaseContext(), MainActivity.class));
            finish();
        }

        return Profile.getCurrentProfile() == null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isLoggedOut();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isLoggedOut();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isLoggedOut();
    }

    @Override
    protected void onStart() {
        super.onStart();
        isLoggedOut();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();

        Wearable.DataApi.removeListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "Connected");
        Wearable.DataApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(TAG, connectionResult.getErrorMessage());
    }

    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        Log.d(TAG, "DataChanged: " + dataEventBuffer.toString());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("FBBFB6C53626BBC62C0C049AA4DF505B")
                .build();

        interstitialAd.loadAd(adRequest);
        interstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                Log.d("Fullscreen ad", "ready");
                interstitialAd.show();
            }
        });
    }

    int adCounter = 0;
    @Override
    public void onBackPressed() {
        adCounter++;
        if(adCounter % 2 != 0) {
            if(interstitialAd.isLoaded()) {
                interstitialAd.show();
            }
        } else {
            super.onBackPressed();
        }
    }
}

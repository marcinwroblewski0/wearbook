package pl.marcinwroblewski.faceviewer;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultTransform;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Marcin Wróblewski on 01.10.2016.
 */

public class WearListener extends WearableListenerService {

    private final String WEARPATH = "/request";
    private JSONArray posts = new JSONArray();
    private final String TAG = "WearListener";
    private GoogleApiClient mGoogleApiClient;
    private Random random;
    private int pagesCount = 0;
    private int requestsCounter = 0;
    private int pagesFeedCollected = 0;

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);

//        if(requestsCounter % 10 == 0 && requestsCounter != 0)
//            showReviewNotification();

        requestsCounter++;

        FacebookSdk.sdkInitialize(getApplicationContext());

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {

                    @Override
                    public void onConnected(Bundle connectionHint) {
                        Log.d(TAG, "onConnected: " + connectionHint);
                        sendInfo(Messages.compose("Syncing"));
                    }
                    @Override
                    public void onConnectionSuspended(int cause) {
                        Log.d(TAG, "onConnectionSuspended: " + cause);
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(ConnectionResult result) {
                        Log.d(TAG, "onConnectionFailed: " + result);
                    }
                })
                // Request access only to the Wearable API
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();

        random = new Random();
        startDataSync();
        Looper.loop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
        Log.d("onDestroy", "called");
    }

    private JSONArray preparePosts(JSONArray posts) {
        Log.d(TAG, "Data: " + posts.toString());
        ArrayMixer arrayMixer = new ArrayMixer();
        posts = arrayMixer.mix(posts);

        ArraySlicer arraySlicer = new ArraySlicer();
        try {
            posts = arraySlicer.slice(posts, 100);
        } catch (JSONException e) {
            Log.e(TAG, "Can't slice posts array");

            sendError(Messages.compose("Can't slice posts array"));
            e.printStackTrace();
        }
        Log.d(TAG, "Mixed and sliced data: " + posts.toString());

        return posts;
    }

    private void sendPosts(JSONArray posts) {
        posts = preparePosts(posts);

        PutDataMapRequest putDataMapReq = PutDataMapRequest.create(DataMapRoutes.POSTS);
        putDataMapReq.getDataMap().putString("posts", posts.toString());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);

        pendingResult.then(new ResultTransform<DataApi.DataItemResult, Result>() {
            @Nullable
            @Override
            public PendingResult<Result> onSuccess(@NonNull DataApi.DataItemResult dataItemResult) {
                Log.d("Posts send", " to watch " + Arrays.toString(dataItemResult.getDataItem().getData()));
                return null;
            }

            @NonNull
            @Override
            public Status onFailure(@NonNull Status status) {
                Log.e("Posts fail", " not sent to watch because " + status.getStatusMessage());
                return super.onFailure(status);
            }
        });

        pagesCount = 0;
        pagesFeedCollected = 0;
        posts = new JSONArray();
    }

    private void sendError(JSONObject error) {

        PutDataMapRequest putDataMapReq = PutDataMapRequest.create(DataMapRoutes.ERRORS);
        putDataMapReq.getDataMap().putString("error", error.toString());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);

        pendingResult.then(new ResultTransform<DataApi.DataItemResult, Result>() {
            @Nullable
            @Override
            public PendingResult<Result> onSuccess(@NonNull DataApi.DataItemResult dataItemResult) {
                Log.d("Error send", " to watch " + Arrays.toString(dataItemResult.getDataItem().getData()));
                return null;
            }

            @NonNull
            @Override
            public Status onFailure(@NonNull Status status) {
                Log.e("Error fail", " not sent to watch because " + status.getStatusMessage());
                return super.onFailure(status);
            }
        });
    }

    private void sendInfo(JSONObject info) {

        PutDataMapRequest putDataMapReq = PutDataMapRequest.create(DataMapRoutes.INFO);
        putDataMapReq.getDataMap().putString("info", info.toString());
        PutDataRequest putDataReq = putDataMapReq.asPutDataRequest();
        PendingResult<DataApi.DataItemResult> pendingResult =
                Wearable.DataApi.putDataItem(mGoogleApiClient, putDataReq);

        pendingResult.then(new ResultTransform<DataApi.DataItemResult, Result>() {
            @Nullable
            @Override
            public PendingResult<Result> onSuccess(@NonNull DataApi.DataItemResult dataItemResult) {
                Log.d("Error send", " to watch " + Arrays.toString(dataItemResult.getDataItem().getData()));
                return null;
            }

            @NonNull
            @Override
            public Status onFailure(@NonNull Status status) {
                Log.e("Error fail", " not sent to watch because " + status.getStatusMessage());
                return super.onFailure(status);
            }
        });
    }


    public void startDataSync() {
        if(Profile.getCurrentProfile() != null) {
            Log.d("fb", Profile.getCurrentProfile().getName());

            //likes
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/likes?limit=2000",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            Log.d("Page", response.getRawResponse() + "");
                            try {
                                JSONObject responseJSON = new JSONObject(response.getRawResponse());
                                JSONArray dataArrayJSON = responseJSON.getJSONArray("data");
                                pagesCount = dataArrayJSON.length() - 1;
                                Log.d(TAG, "Pages count: " + pagesCount);
                                for (int i = 0; i < dataArrayJSON.length(); i++) {
                                    JSONObject page = (JSONObject) dataArrayJSON.get(i);
                                    gatherDataAndSend(page.get("id").toString(), page.get("name").toString());
                                }

                                if(pagesCount <= 0)
                                    sendError(Messages.compose("No posts available"));

                            } catch (JSONException e) {
                                sendError(Messages.compose("Can't decode data from server"));
                                e.printStackTrace();
                            }
                        }
                    }
            ).executeAsync();
        }
    }

    public void gatherDataAndSend(String id, final String name) {
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + id + "/feed",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        handleFeedResponse(response, name);
                    }
                }
        ).executeAsync();
    }

    private void handleFeedResponse(GraphResponse response, String pageName) {
        Log.d("Timeline", response.getRawResponse());
        try {
            JSONArray dataArrayJSON = StringToJSON.makeJSONArray(response.getRawResponse());
            for (int i = 0; i < dataArrayJSON.length(); i++)
                posts.put(JSONToPosts.extractPost(dataArrayJSON.getJSONObject(i), pageName));
        } catch (JSONException e) {

            sendError(Messages.compose("Can't decode data from server"));
            e.printStackTrace();
        }

        pagesFeedCollected++;
        if(pagesFeedCollected == pagesCount) {
            sendPosts(posts);
        }
    }

    public void showReviewNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_thumb_up_button)
                        .setColor(getApplicationContext().getResources().getColor(R.color.colorPrimary))
                        .setContentTitle("I need your opinion")
                        .setContentText("Can you tell me about your experience with Wearbook?");

        Intent intent = new Intent(getApplicationContext(), FormActivity.class);
        mBuilder.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, intent, 0));

        NotificationManagerCompat notificationManager = (NotificationManagerCompat.from(getApplicationContext()));
        notificationManager.notify(1337, mBuilder.build());
    }
}

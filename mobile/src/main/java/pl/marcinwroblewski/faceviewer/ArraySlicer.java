package pl.marcinwroblewski.faceviewer;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by wrbl on 06.10.16.
 */

public class ArraySlicer {

    public JSONArray slice(JSONArray array, int itemsPerSlice) throws JSONException {
        JSONArray slicedArray = new JSONArray();
        for(int i = 0; i < (itemsPerSlice > array.length() ? array.length() - 1 : itemsPerSlice); i++) {
            slicedArray.put(array.get(i));
        }
        return slicedArray;
    }

}

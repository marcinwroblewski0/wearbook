package pl.marcinwroblewski.faceviewer;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Admin on 16.11.2016.
 */

public class LikesList {

    public static void set(Context context, JSONArray ids) {
        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("likes", ids.toString());
        editor.commit();
    }

    public static JSONArray get(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

        if(preferences.contains("likes")) try {
            return new JSONArray(preferences.getString("likes", "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static boolean isAvailble(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        return preferences.contains("likes");
    }
}

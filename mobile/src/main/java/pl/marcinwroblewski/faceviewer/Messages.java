package pl.marcinwroblewski.faceviewer;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by wrbl on 22.11.16.
 */

public class Messages {

    public static JSONObject compose(String message) {
        JSONObject error = new JSONObject();
        try {
            error.put("message", message);
            error.put("timestamp", System.currentTimeMillis());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return error;
    }

}
